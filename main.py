# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pathlib
import argparse
import numpy as np
from matplotlib import pyplot as plt
import lutgen.lutgen as lutg

def get_config() -> dict:
    cfg = {
        'approx_meth': tuple(['sampling', 'fitting']),  # force uniform sampling or -spacing
        'mfunc': {
            'exp2': lutg.exp2,
            'reciprocal': lutg.reciprocal,
            'sqrt': lutg.sqrt,
            'log2': lutg.log2, 
            'logistic': lutg.logistic, 
            'softplus': lutg.softplus,
            'sin': lutg.sin,
            'tanh': lutg.tanh,
            },
    }
    return cfg

def guarantee_safe_config(opts: argparse.Namespace) -> dict:
    fig_res = np.minimum(opts.fig_res,opts.fp_res)
    lut_res = np.minimum(opts.lut_res,opts.fp_res-1)    
    lut_res = np.minimum(opts.lut_res,opts.fig_res)
    approx_res = np.minimum(opts.approx_res + lut_res, opts.fp_res) - lut_res

    options = {
        'mfunc' : opts.mfunc,
        'fp_res' : opts.fp_res,
        'fig_res' : int(fig_res),
        'lut_res' : int(lut_res),
        'approx_res' : int(approx_res),
        'approx_meth' : opts.approx_meth,
        'range_optimize': opts.range_optimize,
        'figure': opts.figure,
        'verbose': opts.verbose,
        'matfile': opts.matfile,
        
    }
    return options


def argument_parsing(cfg: dict) -> argparse.Namespace:

    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] FUNCTION"
    description = script_path.name + " is a LUT generator"
    parser = argparse.ArgumentParser(usage=usage, description=description)

    parser.add_argument("-n", "--fp-res", help="fractional fixed point resolution [bits]", dest="fp_res",
                        action="store", default=23, type=int, choices=list(range(8, 48)), nargs="?")
    parser.add_argument("-k", "--lut-res", help="LUT address length [bits]",
                        dest="lut_res",
                        action="store", default=2, type=int, choices=list(range(2, 47)), nargs="?")
    parser.add_argument("-u", "--up-approx-res", help="upsampling resolution for non-default approximation",
                        dest="approx_res",
                        action="store", default=2, type=int, choices=list(range(0, 16)), nargs="?")
    parser.add_argument("-f", "--fig-res", help="figure fractional resolution",
                        dest="fig_res",
                        action="store", default=23, type=int, choices=list(range(8, 48)), nargs="?")
    parser.add_argument("-a", "--approx-meth", help="Approximation method",
                        dest="approx_meth",
                        action="store", default=cfg['approx_meth'][0], type=str,
                        choices=cfg['approx_meth'], nargs="?")
    parser.add_argument("-R", "--range-optimization", help="optimize range",
                        dest="range_optimize", action="store_true", default=False)
    parser.add_argument("-v", "--verbose", help="verbose mode",
                        dest="verbose", action="store_true", default=False)
    parser.add_argument("-s", "--show-figure", help="show figure",
                        dest="figure", action="store_true", default=False)
    parser.add_argument("-m", "--matfile-dump", help="dump to .mat instead of .txt",
                        dest="matfile", action="store_true", default=False)


    parser.add_argument("mfunc", help="math func to LUT approximate", action="store", default=None,
                        nargs="?", metavar="MFUNC",
                        choices=cfg['mfunc'].keys())

    options = parser.parse_args()

    return options

def main() -> int:

    cfg = get_config()
    options = argument_parsing(cfg)
    opts = guarantee_safe_config(options)
    approx_meth=opts['approx_meth']
    if opts['range_optimize']:
        approx_meth = cfg['approx_meth'][0]

    if opts['verbose']:
        print('-' * 80)
        print(f" **** LUT generator: Input configuration ****")
        print(f"Math function: {opts['mfunc']}(x)")
        print(f"fractional fixed-point resolution [bits]: {opts['fp_res']}")
        print(f"LUT resolution [bits]: {opts['lut_res']}")
        print(f"Approximation method: {opts['approx_meth']}")
        print('-' * 80)
        print(f" **** LUT generator: Design log ****")
    mf = cfg['mfunc'][opts['mfunc']](opts['fp_res'], 
                                     opts['lut_res'],
                                     opts['approx_res'],
                                     approx_meth=approx_meth,
                                     verbose=opts['verbose'],
                                     )
    if opts['range_optimize']:
        mf.optimize_range()
    if opts['approx_meth'] != approx_meth:
        mf.set_approx_meth(opts['approx_meth'])
        mf.redesign_approx()
    
    approx_math_func = mf.get_approximated_func()
    if opts['verbose']:
        print('-'*80)
        print(f" **** LUT generator: Output Report ****")
        print(f"Math function: {opts['mfunc']}(x)")    
        print(f"Approximated math function: {approx_math_func}")
        print(f"fractional fixed-point resolution [bits]: {opts['fp_res']}")
        print(f"LUT resolution [bits]: {opts['lut_res']}")
        print(f"Approximation method: {opts['approx_meth']}")
        print(f"Used scaling: {mf.x_scl}")
        print(f"with A                : {mf.A}")
        print(f"with log2(A)          : {np.log2(mf.A)}")
        print(f"with K                : {mf.K}")
        print(f"with log2(K)          : {np.log2(mf.K)}")
        print(f"Range bounds error    : {mf.e_lim}")
        print(f"Maximal absolute error: {mf.e_amx}")
        print(f"Root-mean-square error: {mf.e_rms}")
        print('-' * 80)
        print()


    mf.lut_dump(opts["matfile"])
    

    if opts['figure']:

        y, ya, x, xa  = mf.calc(opts['fig_res'])

        fig = plt.figure(f"lutgen")
        fig.suptitle(f"f(x) = {approx_math_func}, fix-point {opts['fp_res']+1}bit,  a(x): {opts['approx_meth']}, {opts['lut_res']} bits", fontsize=16)
        axs = fig.subplots(ncols=1, nrows=2)
        axs[0].plot(x, y, label=f"f(x)")
        axs[0].plot(x, ya, linestyle='--' , label=f"a(x)")
        axs[0].legend(loc='best', fontsize=14)
        axs[0].set_xlabel("x", fontsize=14)
        axs[0].set_ylabel("y", fontsize=14)
        axs[0].set_title(f"function: y=f(x), y=a(x)", fontsize=14)
        axs[0].grid()
        axs[1].plot(x, ya - y, label='e(x)')
        axs[1].legend(loc='best', fontsize=14)
        axs[1].set_xlabel("x", fontsize=14)
        axs[1].set_ylabel("e", fontsize=14)
        axs[1].set_title(f"error: e(x) = a(x) - f(x)", fontsize=14)
        axs[1].grid()
        

        
        fig.show()

        # plt.subplot(2,1,1)
        # plt.plot(x, y, label='nat')
        # plt.plot(x, ya, linestyle='--' , label='lut')
        # plt.legend(loc='best')
        # plt.xlabel("in")
        # plt.ylabel("out")
        # plt.grid()
        # plt.subplot(2, 1, 2)
        # plt.plot(x, ya - y, label='err')
        # plt.legend(loc='best')
        # plt.xlabel("in")
        # plt.ylabel("err")
        # plt.grid()
        # plt.show()
        
        
    plt.show()
    return 0

if __name__ == '__main__':
    sys.exit(main())
