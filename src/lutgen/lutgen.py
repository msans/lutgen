from lutgen.mfunc.exp2 import Exp2 as Exp2Lut
from lutgen.mfunc.reciprocal import Reciprocal as ReciprocalLut
from lutgen.mfunc.sqrt import Sqrt as SqrtLut
from lutgen.mfunc.log2 import Log2 as Log2Lut
from lutgen.mfunc.tanh import Tanh as TanhLut
from lutgen.mfunc.logistic import Logistic as LogisticLut
from lutgen.mfunc.softplus import Softplus as SoftplusLut
from lutgen.mfunc.sin import Sin as SinLut

approx_meth_default = 'sampling'
approx_res_default = int(2)

def exp2(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> Log2Lut:
    """
    Return an initialized reciprocal object
    """
    return Exp2Lut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)


def reciprocal(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> Log2Lut:
    """
    Return an initialized reciprocal object
    """
    return ReciprocalLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def sqrt(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> Log2Lut:
    """
    Return an initialized sqrt object
    """
    return SqrtLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def log2(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> Log2Lut:
    """
    Return an initialized log2 object
    """
    return Log2Lut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def tanh(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> TanhLut:
    """
    Return an initialized tanh object
    """
    return TanhLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def logistic(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> LogisticLut:
    """
    Return an initialized logistic object
    """
    return LogisticLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def softplus(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> SoftplusLut:
    """
    Return an initialized softplus object
    """
    return SoftplusLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)

def sin(fp_res: int,
             lut_res: int,
             approx_res=approx_res_default,
             approx_meth=approx_meth_default,
             verbose=False) -> SinLut:
    """
    Return an initialized sin object
    """
    return SinLut(fp_res, lut_res, approx_res=approx_res, approx_meth=approx_meth, verbose=verbose)
