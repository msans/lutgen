
from abc import ABC, abstractmethod
import numpy as np
import pwlf
from scipy.io import savemat as scipy_io_savemat
import csv

approx_meth_default = 'sampling'
approx_res_default = int(2)

class Mfunc(ABC):

    # **************************************************************************
    # abstract methods
    # **************************************************************************

    @abstractmethod
    def reset(self) -> None:
        pass 

    @abstractmethod
    def _design_specific(self) -> None:
        pass

    @abstractmethod
    def _compute_specific(self) -> None:
        pass

    @abstractmethod
    def _design_compute_specific(self) -> None:
        pass

    @abstractmethod
    def _approx_specific(self, x: np.array) -> np.array:
        pass
    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 mfunc: str,
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default, 
                 range_optimizable=True,
                 opt_lb=-2,
                 opt_rb=2, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        
        self.A = 1
        self.K = 1
        self.range_optimizable = range_optimizable
        self.mfunc = mfunc
        self.implicit_in_scl = ''
        self.verbose = verbose
        self.set_approx_meth(approx_meth)
        self.x_rng_update = np.array([False,False])
        self.y_rng_update = np.array([False,False])
        self.force_lbound = False
        self.force_rbound = False

        
        self._set_fp_range(fp_res)  # scaled fixed point input range

        self._lut_create(lut_res, approx_res + lut_res)

        self._set_optimization_subrange(opt_lb,opt_rb)

    # **************************************************************************
    # public methods
    # **************************************************************************
    def get_approximated_func(self):
        
        y_scl = self.y_scl
        if y_scl == np.round(y_scl):
            y_scl = int(y_scl)
        A = self.A
        log2A = np.log2(A)
        print_A = False
        print_log2A = False
        A_str = ''
        if log2A == np.round(log2A):
            # A positive or negative power of 2
            if log2A != 0:
                print_log2A = True
                A_str = f"2^{int(log2A)}"
        else:
            if A == np.round(A):
                # A integer
                if A != 1:
                    print_A = True
                    A_str = str(int(A))
            else: # A != np.round(A) and log2A != np.round(log2A):
                print_A = True
                A_str = str(A)

        y_str = ''
        if y_scl != 1 :
            if print_A or print_log2A:
                y_str = f"{A_str}/{y_scl} "
            else:
                y_str = f"1/{y_scl} "
        else:
            y_str = f"{A_str} "
        
        in_scl = self.K * self.x_scl
        if in_scl == np.round(in_scl):
            in_scl = int(in_scl)
        x_str = ''
        if in_scl != 1:
            x_str = f"{in_scl}"

        return f"{y_str}{self.mfunc}({self.implicit_in_scl}{x_str}x)"

    def set_approx_meth(self, approx_meth=approx_meth_default) -> None:
        self.approx_meth = approx_meth


    def set_range(self, x_rng: np.array, y_rng:np.array) -> None:
        self.x_rng = x_rng
        self.y_rng = y_rng
        self._update()

    def optimize_range(self) -> None:
        if not(self.range_optimizable):
            return
        it = 1
        scl = np.log2(self.x_scl)
        it_max = np.abs(scl)
        scl = -np.sign(scl)
        conf_approx_meth = self.approx_meth # quick method during iteration
        self.set_approx_meth() # quick method during iteration
        while (self.e_amx * 0.05 > self.e_lim and it < it_max): 
            if self.verbose:
                msg = f"Iterative Lut range optimization"
                msg += f", method: {self.approx_meth}"
                msg += f" (iteration {it})"
                print(msg)

            # dynamically constrain input range
            self._rescale_range(2**(scl))
            if self.verbose:
                msg = f"  Input range: [{self.x_rng[0]},{self.x_rng[1]}]\n"
                msg += f"  Input scaling: {self.x_scl}\n"
                msg += f"  Output range: [{self.y_rng[0]},{self.y_rng[1]}]\n"
                msg += f"  Output scaling {self.y_scl}"
                print(msg) 

            self._approx()
            it = it + 1
        
        if conf_approx_meth == self.approx_meth:
            return None
        self.set_approx_meth(conf_approx_meth)
        if self.verbose:
            msg = f"Lut approximation optimization"
            msg += f", method: {self.approx_meth}"
            msg += f" (please wait ...)"
            print(msg)
        self._design_specific()
        self._approx()

    def redesign_approx(self) -> None:
        self._design_specific()
        self._approx()


    def calc(self, c_res) -> (np.array, np.array, np.array, np.array):
        x = np.arange(self.x_rng[0],self.x_rng[-1],self._res2step(c_res)*self.x_scl)
        y = self._compute_specific(x)
        xa_rng = self.x_rng/self.x_scl
        xa = np.arange(xa_rng[0],xa_rng[-1],self._res2step(c_res))
        #ya = self._approx_common(x)
        ya = self._approx_specific(x)

        return y, ya, x, xa

    def lut_dump(self, matfile=False) -> None:
        filestem = "_".join([
            f"{self.mfunc}",
            f"n{self.fp_res}",
            f"k{self.lut_res}",
            f"{self.approx_meth}",
            f"xscl-{self.x_scl}",
            f"yscl-{self.y_scl}", ])
        if matfile:
            self._lut_mat_dump(filestem)
        #self._lut_txt_dump(filestem)
        self._lut_csv_dump(filestem)
        return None

    # **************************************************************************
    # static private methods (computing)
    # **************************************************************************
    
    @staticmethod
    def _res2size(res: int) -> int:
        return 2**res

    @staticmethod
    def _res2step(res: int) -> float:
        return 2**(-float(res))

    @staticmethod
    def _get_rng_scaling(rng: np.array) -> int:
        return int(np.round(np.max(np.abs(rng))))

    @staticmethod
    def _quantize(x: np.array, qstep: float) -> np.array:
        return np.minimum(1-qstep,np.maximum(-1, np.round(x / qstep) * qstep))

    @staticmethod
    def _compute_error(ya: np.array, y: np.array) -> (
            np.array, np.array, np.array):
        e_a = ya - y
        e_amx = np.max(np.abs(e_a))
        e_rms = np.sqrt(np.mean(e_a**2))

        return e_a, e_amx, e_rms

    # *************************************************************************
    # private methods (general)
    # **************************************************************************

    def _set_fp_range(self, res: int) -> None:
        self.fp_res = res
        self.fp_nelem = self._res2size(self.fp_res+1)
        self.fp_nelem_half = self._res2size(self.fp_res)
        self.fp_qstep = self._res2step(self.fp_res)
        self.x_fp = np.arange(-1,1,self.fp_qstep)
        self.x_fp_rng = np.array([self.x_fp[0],self.x_fp[-1]])
        self.x_fp_len = self.x_fp_rng[-1] - self.x_fp_rng[0]


    def _lut_create(self, res: int, apx_res: int) -> None:
        self.lut_res = np.maximum(2,np.minimum(res, self.fp_res))
        self.lut_qstep = self._res2step(self.lut_res)
        self.lut_nelem = self._res2size(self.lut_res)
        self.lut_nelem_half = self._res2size(self.lut_res-1)
        self.lut_x_idx_rclosed = np.arange(0, self.lut_nelem+1, 1).astype(int)
        self.lut_x_idx = self.lut_x_idx_rclosed[:-1]
        self.lut_x_idx_addr0 = self.lut_nelem_half  
        self.lut_x_addr_rclosed = self.lut_x_idx_rclosed - self.lut_x_idx_addr0
        self.lut_x_addr = self.lut_x_addr_rclosed[:-1]
        self.lut_x_norm_rclosed = self.lut_x_addr_rclosed / self.lut_nelem_half
        self.lut_x_norm = self.lut_x_norm_rclosed[-1]
        self.apx_res = apx_res
        self.apx_nelem = self._res2size(self.apx_res)
        self.apx_nelem_half = self._res2size(self.apx_res-1)
        self.apx_x_idx_rclosed = np.arange(0, self.apx_nelem+1, 1).astype(int)
        self.apx_x_addr_rclosed = self.apx_x_idx_rclosed - self.apx_nelem_half
        self.apx_x_norm_rclosed = self.apx_x_addr_rclosed / self.apx_nelem_half

    def _set_optimization_subrange(self,opt_lb: float,opt_rb: float) -> None:
        if opt_lb >= -1:
            self.force_rbound = True
        else:
            opt_lb = -1
        if opt_rb <= 1:
            self.force_rbound = True
        else:
            opt_rb = 1
        # FIXME assert opt_lb <= opt_rb + self.lut_qstep
        idx_l = np.ceil(opt_lb * self.lut_nelem_half)
        idx_r = np.floor(opt_rb * self.lut_nelem_half)
        idx_l = np.maximum(-self.lut_nelem_half,idx_l)
        idx_r = np.minimum(idx_r,self.lut_nelem_half - 1)
        idx_l = np.minimum(idx_l,idx_r - 1)
        idx_r = np.maximum(idx_l + 1, idx_r)
        self.lut_x_idx_sub_rclosed = np.arange(idx_l,idx_r+2,1).astype(int) + self.lut_nelem_half
        c = int(2**(self.apx_res - self.lut_res))
        self.apx_x_idx_sub = np.arange(idx_l*c,(idx_r+1)*c,1).astype(int) + self.apx_nelem_half

    def _get_error_subrange(self) -> np.array:
        idx_l = np.ceil(self.x_rng[0] * self.fp_nelem_half)
        idx_r = np.floor(self.x_rng[1] * self.fp_nelem_half)
        idx_l = np.maximum(-self.fp_nelem_half,idx_l)
        idx_r = np.minimum(idx_r,self.fp_nelem_half - 1)
        idx_l = np.minimum(idx_l,idx_r - 1)
        idx_r = np.maximum(idx_l + 1, idx_r)
        return np.arange(idx_l,idx_r+1,1).astype(int) + self.fp_nelem_half
        

    def _set_irange_magnitude(self) -> None:
        self.mx_rng = np.max(np.abs(self.x_rng))


    def _compute(self) -> None:
        self.y = self._compute_specific(self.x)


    def _compute_samples(self) -> None:
        self.lut_x_rclosed = self.lut_x_norm_rclosed * self.mx_rng
        self.lut_x = self.lut_x_rclosed[:-1]
        self.lut_y_rclosed = self._design_compute_specific(self.lut_x_rclosed)


    def _reset_io(self) -> None:
        self.x = self.x_fp*self.mx_rng
        self._compute()
        self._compute_samples()


    def _update(self) -> None:
        self._set_irange_magnitude()
        self._reset_io()
        self.x_scl = self._get_rng_scaling(self.x_rng)
        self.y_scl = self._get_rng_scaling(self.y_rng)
        self._design_specific()

    
    def _rescale_range(self, r: float) -> None:
        for k in range(0,2):
            if self.x_rng_update[k]:
                self.x_rng[k] = self.x_rng[k]*r
            if self.y_rng_update[k]:
                self.y_rng[k] = self.y_rng[k]*r
        self._update()  


    def _approx_common(self, x: np.array) -> np.array:
        x_addr, x_seg = self._lut_get_addr(x)
        return self._lut_pwl(x_addr, x_seg)  * self.y_scl


    def _approx(self) -> None:
        self.y_a = self._approx_specific(self.x)
        idx = self._get_error_subrange()
        self.e_a, self.e_amx, self.e_rms = self._compute_error(self.y_a[idx], self.y[idx])
        #self.e_a, self.e_amx, self.e_rms = self._compute_error(self.y_a, self.y)
        #self.e_lim = np.max(np.abs(self.y_rng-self._design_compute_specific(self.x_rng)))
        self.e_lim = np.max(np.abs(self.y_rng-self._approx_specific(self.x_rng)))
        if self.verbose:
            print(f"  Range bounds error    : {self.e_lim}")
            print(f"  Maximum absolute error: {self.e_amx}")
            print(f"  Root-mean-square error: {self.e_rms}")


    def _safe_init(self):
        self.reset()
        self._approx()


    # *************************************************************************
    # private methods (lut design)
    # **************************************************************************  


    def _lut_quantize(self):
        self.lut_y = self._quantize(self.lut_y, self.fp_qstep)
        self.lut_s = self._quantize(self.lut_s, self.fp_qstep)

    def _lut_design(self) -> None:
        y_pred = self.lut_y_rclosed
        if self.approx_meth == 'fitting':
            x = self.apx_x_norm_rclosed * self.mx_rng
            #y = self._compute_specific(x)
            y = self._design_compute_specific(x)
            my_pwlf = pwlf.PiecewiseLinFit(x[self.apx_x_idx_sub], y[self.apx_x_idx_sub])
            my_pwlf.fit_with_breaks(self.lut_x_rclosed[self.lut_x_idx_sub_rclosed])
            y_pred_sub = my_pwlf.predict(self.lut_x_rclosed[self.lut_x_idx_sub_rclosed])
            if self.force_rbound:
                y_pred_sub[0] = self.lut_y_rclosed[self.lut_x_idx_sub_rclosed[0]]
            if self.force_lbound:
                y_pred_sub[-1] = self.lut_y_rclosed[self.lut_x_idx_sub_rclosed[-1]]
            y_pred[self.lut_x_idx_sub_rclosed] = y_pred_sub
            
        
        y_pred = y_pred / self.y_scl
        # offset:
        self.lut_y = y_pred[:-1] 
        # slope:
        # s=(y[k+1]-y k])/(x[k+1]-x[k]),
        # with uniform sampling: x[k+1]-x[k] = x[1]-x[0]
        # => using re-scaled slope s*(x[1]-x[0])
        # => using normalized re-scaled slope s*(x[1]-x[0]) / 2**(lut_res-1)
        self.lut_s = np.diff(y_pred,n=1,axis=-1) #/ self.lut_nelem_half
        self._lut_quantize()

    # *************************************************************************
    # private methods (lut usage)
    # **************************************************************************  

    def _lut_get_addr(self, x: np.array) -> (np.array, np.array):
        xq = x/self.x_scl*self.lut_nelem_half
        x_addr = np.minimum(self.lut_x_addr[-1],np.maximum(self.lut_x_addr[0], 
                                                           np.floor(xq)))
        return x_addr.astype(int), xq - x_addr

    def _lut_pwl(self, x_addr: np.array, x_seg: np.array) -> np.array:
        idx = x_addr + self.lut_x_idx_addr0
        return self.lut_y[idx] + x_seg * self.lut_s[idx] #* self.lut_nelem_half

    # **************************************************************************
    # private methods (lut dump)
    # **************************************************************************  

    def _lut_mat_dump(self, filestem: str) -> None:
        v = np.hstack((self.lut_y.reshape([self.lut_nelem,1]), 
                       self.lut_s.reshape([self.lut_nelem,1])))
        matfile_content = {
            'lut': v,   
            'col_labels': ['offset', 'slope'],
            'x_scaling': self.x_scl,
            'y_scaling': self.y_scl,
        }
        scipy_io_savemat(filestem+".mat", matfile_content)

        return None

    def _lut_txt_dump(self, filestem: str) -> None:

        lines = list()
        lines.append('// LUT x-axis scaling factor:')
        lines.append(str(self.x_scl))
        lines.append('// LUT y-axis scaling factor:')
        lines.append(str(self.y_scl))
        lines.append('// LUT content: address, offset, slope')
        
        v = np.hstack((self.lut_y.reshape([self.lut_nelem,1]), 
                       self.lut_s.reshape([self.lut_nelem,1])))

        lines.extend([','.join([str(r[0]),str(r[1])]) for r in v])

        with open(filestem + ".txt", "w") as f:
            f.write("\n".join(lines))

        return None

    def _lut_csv_dump(self, filestem: str) -> None:

        v = np.hstack((self.lut_y.reshape([self.lut_nelem,1]), 
                       self.lut_s.reshape([self.lut_nelem,1])))

        header_labels = [
            'resolution',
            'nelem',
            'addr0',
            'idx_start',
            'idx_stop',
            'xscl',
            'yscl',
            'mx_rng',
        ]

        header_values = [
            self.lut_res, 
            self.lut_nelem,
            self.lut_x_idx_addr0,
            0,
            self.lut_nelem,
            self.x_scl,
            self.y_scl,
            self.mx_rng,
        ]

        with open(filestem+".csv", 'w') as f:
            writer = csv.writer(f)
            writer.writerow(header_labels)
            writer.writerow(header_values)
            writer.writerow(['y_k','s_k'])
            writer.writerows(v)

        return None
