# **************************************************************************
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg
from lutgen.utils import fpbase2 as lutg_fpbase2

approx_meth_default = 'sampling'
approx_res_default = int(2)
range_optimizable_default = False
optimization_lower_bounds_default = 0.5
optimization_upper_bounds_default = 1

class Reciprocal(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default,
                 range_optimizable=range_optimizable_default,
                 opt_lb=optimization_lower_bounds_default,
                 opt_rb=optimization_upper_bounds_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('reciprocal',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth,
                         range_optimizable=range_optimizable,
                         opt_lb=opt_lb,
                         opt_rb=opt_rb,  
                         verbose=verbose)
        
        self.A = self.fp_qstep
        self._safe_init()
        
        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        x_lb = self.fp_qstep
        # y_scl:upper bound double precision: power signal to dB
        y_lb = 1
        y_rb = self.fp_qstep
        x_rb = 1
        if self.verbose:   
            print(f"Safe range, raw: [{x_lb},{x_rb}]")
        self.set_range(np.array([x_lb, x_rb]), [y_lb, y_rb])
        return None

    # **************************************************************************
    # private methods
    # **************************************************************************

    def _reciprocal(self, x: np.array) -> np.array:
        return 1/x

    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        x_low_sat = np.maximum(self.x_rng[0],x)
        y = 2**(-self.fp_res)*self._reciprocal(x_low_sat)
        return np.minimum(1 - 2**(-1-self.fp_res),y)

    def _design_compute_specific(self, x: np.array) -> None:
        x_low_sat = np.maximum(self.x_rng[0],x)
        y = 2**(-1)*self._reciprocal(x_low_sat)
        return np.minimum(1,y)

    def _approx_specific(self, x: np.array) -> np.array:
        s, m, e = lutg_fpbase2(np.maximum(self.fp_qstep,x), self.fp_res)
        #return self._approx_common(m) * 2**(e+self.fp_res-1)        
        #return self._approx_common(m) 
        return self._approx_common(m) * 2**(-((self.fp_res-1)+e))
