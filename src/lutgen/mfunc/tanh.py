# **************************************************************************
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg

approx_meth_default = 'sampling'
approx_res_default = int(2)

class Tanh(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('tanh',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth, 
                         verbose=verbose)
        
        self._safe_init()
        
        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        y_rng = np.array([-1,1])  # specific output range
        # input range
        # tanh is central symetric around (0,0) 
        # -> left bound is the negative of right bound
        # condition for right bound: 
        # asympt err for y-> inf: tanh(inf)-tanh(x_q) <= q/2
        # 1-logistic(x_q) <= q/2
        # x_q <= reciprocal_tanh(1-q/2)
        x_b = self._reciprocal_compute(y_rng[1] - self.fp_qstep/2)
        if self.verbose:   
            print(f"Safe range, raw: [{-x_b},{x_b}]")
            print(f"(with {self.mfunc}(+inf) - {self.mfunc}(x_b) <= (2**{-self.fp_res})/2)")
        x = 2**(np.ceil(np.log2(x_b)))
        if self.verbose:
            print(f"Safe range, next power of 2: [{-x},{x}]")
        self.set_range(np.array([-x, x]), y_rng)
        self.x_rng_update = np.array([True,True])
        return None

    # **************************************************************************
    # private methods
    # **************************************************************************

    @staticmethod
    def _reciprocal_compute(x: np.array) -> np.array:
        return 1/2*np.log((1+x)/(1-x))  # aka artanh(x)
    
    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        return (np.exp(x) - np.exp(-x)) / (np.exp(x) + np.exp(-x))

    def _design_compute_specific(self, x: np.array) -> None:
        return self._compute_specific(x)
    
    def _approx_specific(self, x: np.array) -> np.array:
        return self._approx_common(x)
