# **************************************************************************
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg
from lutgen.utils import fp_int_frac as lutg_fp_int_frac

approx_meth_default = 'sampling'
approx_res_default = int(2)
range_optimizable_default = False
optimization_lower_bounds_default = 0
optimization_upper_bounds_default = 1

class Exp2(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default,
                 range_optimizable=range_optimizable_default,
                 opt_lb=optimization_lower_bounds_default,
                 opt_rb=optimization_upper_bounds_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('exp2',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth,
                         range_optimizable=range_optimizable,
                         opt_lb=opt_lb,
                         opt_rb=opt_rb,  
                         verbose=verbose)
        
        self.K = 2**np.ceil(np.log2(self.fp_res))
        self._safe_init()
        


        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        
        x_lb = -1
        # y_scl:upper bound double precision: power signal to dB
        y_lb = 0
        y_rb = 1
        x_rb = 0
        self.set_range(np.array([x_lb, x_rb]), [y_lb, y_rb])
        return None

    # **************************************************************************
    # private methods
    # **************************************************************************

    def _exp2(self, x: np.array) -> np.array:
         return 2**(self.K*x)

    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        return self._exp2(np.minimum(self.x_rng[1],x))

    def _design_compute_specific(self, x: np.array) -> None:
        return 2**(x)

    def _approx_specific(self, x: np.array) -> np.array:
        a = self.K* np.minimum(self.x_rng[1],np.maximum(self.x_rng[0],x))
        ai, af = lutg_fp_int_frac(a)  # ai <=0, af in [0,1[
        u = ai
        v = af - 1
        q = 2
        return q*2**u*self._approx_common(v)
