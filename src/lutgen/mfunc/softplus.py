
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg

approx_meth_default = 'sampling'
approx_res_default = int(2)

class Softplus(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default,
                 approx_meth=approx_meth_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('softplus',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth, 
                         verbose=verbose)
        
        self._safe_init()
        
        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        # output range
        y_rng = np.array([0, np.inf])  # specific output range
        self.y_rng_update = np.array([False,True])
        # left bound for y(x) = softplus(x)
        # for x-> -inf, y(x) -> 0+, and with ya >=0
        # => y(-inf)-ya(x_lb) <= 0
        # => safe approx error in [-q/2,0]
        e_safe = self.fp_qstep / 2
        x_lb = self._reciprocal_compute(y_rng[0] + e_safe)
        # right bound for y(x) = softplus(x), equiv to
        # right bound for u(x) = y(x) - x = softplus(x) - x
        # left bound for z(x) = y(-x) + x = softplus(-x) + x = softplus(x)
        # proof: 
        # z = x+y(-x)=x+log(1+exp(-x)) 
        # <=> exp(z) = exp(x+log(1+exp(-x))) = exp(x)*exp(log(1+exp(-x)))
        # exp(z) = exp(x)*(1+exp(-x)) = exp(x) + 1 
        # <=> z = log(1+exp(x)) = y
        # =>  so x_rb = -x_lb
        x_b = abs(x_lb)
        if self.verbose:
            print(f"Safe range, raw: [{-x_b},{x_b}]")
            print(f"(with {self.mfunc}(+inf) - {self.mfunc}(x_b) <= (2**{-self.fp_res})/2)")
        x = 2**(np.ceil(np.log2(x_b)))
        if self.verbose:
            print(f"Safe range, next power of 2: [{-x},{x}]")
        y_rng[1] = x
        self.set_range(np.array([-x, x]), y_rng)
        self.x_rng_update = np.array([True,True])
        return None


    # **************************************************************************
    # private methods
    # **************************************************************************

    @staticmethod
    def _reciprocal_compute(x: np.array) -> np.array:
        return np.log(- 1 + np.exp(x)) # for x >= 0

    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        return np.log(1 + np.exp(x))

    def _design_compute_specific(self, x: np.array) -> None:
        return self._compute_specific(x)

    def _approx_specific(self, x: np.array) -> np.array:
        return self._approx_common(x)
