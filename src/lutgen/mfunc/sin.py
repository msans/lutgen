# **************************************************************************
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg

approx_meth_default = 'sampling'
approx_res_default = int(2)

class Sin(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('sin',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth, 
                         verbose=verbose)
        
        self.implicit_in_scl = 'pi '
        self._safe_init()
        
        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        y_rng = np.array([-1,1])  # specific output range
        x_rng = np.array([-1,1])  # specific input range
        if self.verbose:   
            print(f"Safe range, raw: [{-x_rng[0]},{x_rng[1]}]")
        self.set_range(x_rng, y_rng)
        self.x_rng_update = np.array([False,False])
        return None

    # **************************************************************************
    # private methods
    # **************************************************************************

    @staticmethod
    def _reciprocal_compute(x: np.array) -> np.array:
        return np.arcsin(x)/np.pi
    
    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        return np.sin(np.pi*x)

    def _design_compute_specific(self, x: np.array) -> None:
        return self._compute_specific(x)
    
    def _approx_specific(self, x: np.array) -> np.array:
        return self._approx_common(x)
