# **************************************************************************
import numpy as np
from lutgen.mfuncabstract import Mfunc as Mfunc_lutg
from lutgen.utils import fpbaseb as lutg_fpbaseb

approx_meth_default = 'sampling'
approx_res_default = int(2)
range_optimizable_default = False
optimization_lower_bounds_default = 0.25
optimization_upper_bounds_default = 1

class Sqrt(Mfunc_lutg):

    # **************************************************************************
    # constructor
    # **************************************************************************

    def __init__(self, 
                 fp_res: int,
                 lut_res: int, 
                 approx_res=approx_res_default, 
                 approx_meth=approx_meth_default,
                 range_optimizable=range_optimizable_default,
                 opt_lb=optimization_lower_bounds_default,
                 opt_rb=optimization_upper_bounds_default, 
                 verbose=False,
                 ) -> None:
        """Constructor
        """
        super().__init__('sqrt',
                         fp_res,
                         lut_res,
                         approx_res=approx_res, 
                         approx_meth=approx_meth,
                         range_optimizable=range_optimizable,
                         opt_lb=opt_lb,
                         opt_rb=opt_rb,  
                         verbose=verbose)
        
        self._safe_init()
        
        return None

    # **************************************************************************
    # public methods
    # **************************************************************************

    def reset(self) -> None:
        """
        """
        x_lb = 0
        # y_scl:upper bound double precision: power signal to dB
        y_lb = 0
        y_rb = 1
        x_rb = 1
        if self.verbose:   
            print(f"Safe range, raw: [{x_lb},{x_rb}]")
        self.set_range(np.array([x_lb, x_rb]), [y_lb, y_rb])
        return None

    # **************************************************************************
    # private methods
    # **************************************************************************

    def _sqrt(self, x: np.array) -> np.array:
         return np.sqrt(x)

    def _design_specific(self) -> None:
        self._lut_design()

    def _compute_specific(self, x: np.array) -> None:
        return self._sqrt(np.maximum(self.x_rng[0],x))

    def _design_compute_specific(self, x: np.array) -> None:
        return self._compute_specific(x)

    def _approx_specific(self, x: np.array) -> np.array:
        s, m, e = lutg_fpbaseb(np.maximum(self.fp_qstep,x), self.fp_res, 4)
        return self._approx_common(m) * 2**(e)
