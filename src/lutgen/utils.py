import numpy as np

def logb(x: np.array, b: int) -> np.array:
    # FIXME assert b
    return np.log2(x)/np.log2(b)

def fpbaseb(x: np.array, q: int, b: int) -> (np.array, np.array, np.array):
# FIXME: assert q>=1
    Q = 2**(-q)
    sgn = np.sign(x)
    m = np.abs(x)
    z = m < Q/2;
    exp = (np.floor(logb(np.maximum(Q,m), 4))+1) * (1-z) + z*(-q)
    man = m * b**(-exp)
    return sgn, man, exp
def fpbase2(x: np.array, q: int) -> (np.array, np.array, np.array):
    # FIXME: assert q>=1
    Q = 2**(-q)
    sgn = np.sign(x)
    m = np.abs(x)
    z = m < Q/2;
    exp = (np.floor(np.log2(np.maximum(Q,m)))+1) * (1-z) + z*(-q)
    man = m * 2**(-exp)
    return sgn, man, exp

def fp_int_frac(x: np.array) -> (np.array, np.array):
    xi = np.floor(x)
    xf = x - xi
    return xi, xf
